---
layout: post
title: Agile fluency model
description: Cesta agilních týmů za úspěchem 

permalink: /blog/agile-fluency-model
cover: /assets/images/2019-02-21/cover.jpg
image: /assets/images/2019-02-21/cover.jpg
---

S agilním vývojem se už každý z nás někdy setkal. Přece jen je to způsob, kterým dnes většina organizací a týmů operuje, pokud nemusí z nějakých konkrétních důvodů volit [FTFP](http://www.web-integration.info/cs/blog/agilni-projekty-zpohledu-zakaznika/){:target="_blank" rel="noopener noreferrer nofollow"} (fix time - fix price). Samozřejmě FTFP přímo nevynucuje [vodopádový model](https://en.wikipedia.org/wiki/Waterfall_model){:target="_blank" rel="noopener noreferrer nofollow"} vývoje, avšak nějaké velké adaptace v průběhu se dělají těžce a často se musí zakomponovat jako vícepráce. Záleží na mnoha faktorech jako fáze projektu, otevřenost klienta, rozsah potřebných změn, velikost rezervy v budgetu atd. I v případě FTFP však volíme agilní praktiky, které snižují riziko nespokojenosti zákazníka při předání, zvyšují efektivitu a kvalitu vývoje a v mnoha případech i samotnou radost z vývoje. Agilní způsob vývoje je zkrátka dnešním standardem úspěšných týmů.

## Agilní manifesto

O [agilním manifestu](https://agilemanifesto.org/){:target="_blank" rel="noopener noreferrer nofollow"} jste již také určitě slyšeli. Sformovala jej skupina známých vývojářů jako Martin Fowler, Robert C. Martin, Kent Beck a další. 

* **Jednotlivci a interakce** před procesy a nástroji
* **Fungující software** před obsáhlou dokumentací
* **Spolupráce se zákazníkem** před sjednáním smluv
* **Reakce na změnu** před dodržováním plánu

Cílem agilního manifesta je zdůraznit to, na co by se týmy měly zaměřit. Rozhodně nelze říci, že nemáme dělat dokumentaci nebo neuzavírat smlouvy. Nepoužívat nástroje a nedefinovat procesy. Můžeme ale říci, že smysluplnost práce má přednost před rigorózním dodržováním pravidel definovaných na začátku projektu, kde je vysoká míra **entropie** - neurčitosti a neznalosti. Adaptace v průběhu vývoje na rozšiřující se znalosti o projektu a potřebách klienta je klíčová pro dodání kvalitního softwaru.

Někteří lidé si agilní přístup k vývoji vykládají dost punkově, že stačí intuice a vše se nějak vyřeší za pochodu, že není potřeba zapojit nějaké metodiky a techniky. Bohužel ale intuitivní přístup nelze rozvíjet a řídit, je to taková **kamikaze** metoda. Jednou jsem četl krásný citát:
> Profesionalismus je to, že děláme svou práci, i když se nám nechce.

Problém je, že když se nám nechce, jsme jakkoli indisponováni, dochází k chybám v úsudku a chování. Přesto některým zbytečným chybám můžeme zabránit díky **systematičnosti**.

## Systematický přístup

Taková hezká definice systematického přístupu zní:
> Metodický přístup, který je opakovatelný a naučitelný krok za krokem.

Cílem je identifikovat nejefektivnější způsob generování konzistentních a optimálních výsledků. Systematický přístup přináší hned několik výhod.

### Transparentnost

Díky systematickému přístupu jsme schopni určit v jaké fázi procesu se nacházíme, co jsme již udělali a co nás ještě čeká. Jsme schopni říci, kdo na čem zrovna dělá, a kdo za co zodpovídá. Zkrátka víme, na čem jsme.

### Redukce předpojatosti

V manažerské psychologii je jedno velice zajímavé téma - [sociální percepce](https://cs.wikipedia.org/wiki/Soci%C3%A1ln%C3%AD_percepce){:target="_blank" rel="noopener noreferrer nofollow"}. V podstatě jde o to, že náš mozek přijaté informace neinterpretuje vždy objektivním způsobem. Některé filtruje a některé zkresluje, proto pravda jednoho nemusí být pravdou druhého. Věci jako [kauzální atribuce](https://wikisofia.cz/wiki/Kauz%C3%A1ln%C3%AD_atribuce){:target="_blank" rel="noopener noreferrer nofollow"}, [haló efekt](https://wikisofia.cz/wiki/Hal%C3%B3_efekt), efekt novosti a další.

Nebo také [informační bias](https://cs.wikipedia.org/wiki/Informa%C4%8Dn%C3%AD_bias){:target="_blank" rel="noopener noreferrer nofollow"}, kognitivní zkreslení ovlivňující naše vyhodnocování a rozhodování.

Systematičnost nám může pomoci odbourat předpojatost a redukovat její dopad. Proto je běžně používaná výslechová metoda [5 whys](https://www.mindtools.com/pages/article/newTMC_5W.htm){:target="_blank" rel="noopener noreferrer nofollow"}, nebo novinářská metoda [5 Ws](https://en.wikipedia.org/wiki/Five_Ws){:target="_blank" rel="noopener noreferrer nofollow"}.

### Opakovatelnost

Jednou z praktik [pragmatického programátora](https://www.amazon.com/Pragmatic-Programmer-Journeyman-Master/dp/020161622X){:target="_blank" rel="noopener noreferrer nofollow"} je **přemýšlet o své práci** - nejezdit na autopilota, ale **ustavičně přemýšlet o tom, co děláme, zatímco to děláme**. Pokud ale jednáme nevědomě, potřebujeme nějaký systém, metodiku, o kterou se můžeme opřít. Díky systematičnosti tak můžeme zopakovat nějaký proces vědomě i nevědomě, což snižuje riziko chybovosti.

## Model

> All models are wrong, but some models are useful
>
> -- Goerge Box

Nerad bych zde obhajoval přínos a důležitost modelů, teorie a literatury. To si schovám na samotný článek ohledně [HEXACO modelu](http://www.hexaco.org/scaledescriptions){:target="_blank" rel="noopener noreferrer nofollow"} a faktoru svědomitosti (viz [Tai Lopez \| Why Grinding Isn't Enough](https://youtu.be/jX5eajzLJMU){:target="_blank" rel="noopener noreferrer nofollow"}).

Odkáži zde pouze na GOTO přednášku [Programming across paradigms](https://youtu.be/Pg3UeB-5FdA){:target="_blank" rel="noopener noreferrer nofollow"}, kde přednášející vysvětluje životní cyklus modelů a jejich přínos i přes jejich nedokonalost.

## Agile fluency model

![agile fluency model](/assets/images/2019-02-21/model.jpg){:target="_blank" rel="noopener noreferrer nofollow"}

Jedná se o model popisující cestu agilních týmů skrze 4 zóny. Každá zóna přináší nějaké výhody a obnáší nějaké investice a změny. Samotné zóny a jejich pořadí bylo vypozorováno autory, kteří se zabývají zaváděním agilního vývoje do organizací a týmů. Není přímo cílem dojít postupně až k poslední zóně, ale zvolit si takovou, která reflektuje cíle a potřeby dané organizace a týmu. Například tým zabývající se tvorbou produktů docílí lepších výsledků ve třetí zóně, kdežto pro tým zabývající se realizací projektů by to bylo moc a docílí lepších výsledků v zóně druhé. Záleží na podstatě poskytovaných služeb a toho, co produkujeme.

Každá zóna závisí na několika agilních dovednostech, pozorovatelných chováních, která vedou k výhodám dané zóny. Nejdůležitější dovedností je **plynulost (fluency)** - schopnost projevovat dovednost za všech okolností, i pod tlakem.

Agilní vývoj je týmová hra, takže plynulost je vlastnost celého týmu, nejen jednotlivých členů. Některé dovednosti jsou však otázkou jen některých jedinců. Každopádně týmová plynulost přichází se schopností sebeorganizace členů týmu, čímž jsou jednotlivé schopnosti aplikovány na správný problém ve správný čas.

### Focusing (Agile fundamentals)

Tým v této zóně produkuje obchodní hodnotu. Pracuje se společnými cíli a přemýšlí nad dodáním hodnot [stakeholderům](https://platinumedge.com/blog/stakeholders-agile-project-management){:target="_blank" rel="noopener noreferrer nofollow"} míst nad technologiemi. Pro úspěšný přechod do této zóny je důležitá **změna týmové kultury**. Členové týmu se musí naučit přemýšlet nad obchodním dopadem a převzít zodpovědnost za úspěch celého týmu. Je zde také důležitá [psychologická bezpečnost](https://slackhq.com/psychological-safety-building-trust-teams){:target="_blank" rel="noopener noreferrer nofollow"}, [týmová pravidla](https://www.mindtools.com/pages/article/newTMM_95.htm){:target="_blank" rel="noopener noreferrer nofollow"} (team charters), skupinové učení a osobní zpětné vazby. 

#### Výhody

Největší výhodou je transparentnost týmové práce a progressu. Dochází zde k lepší a efektivnější kooperaci a komunikaci týmu. Také jsme schopni reagovat na změny podle potřeby. Proto se tato zóna také jmenuje **Agile fundamentals**.

#### Nástroje a investice

Asi nikoho nepřekvapí, že základními nástroji pro tuto zónu jsou [Scrum](https://www.atlassian.com/agile/scrum){:target="_blank" rel="noopener noreferrer nofollow"}, [Kanban](https://www.atlassian.com/agile/kanban){:target="_blank" rel="noopener noreferrer nofollow"} a [Extreme programming](http://www.extremeprogramming.org/){:target="_blank" rel="noopener noreferrer nofollow"} (netechnický). Důležité je také mít full-time členy týmu, aby mohli růst společně s projektem. Pro usnadnění kooperace a komunikace je dobré dedikovat společné fyzické pracoviště. Dalšími vhodnými nástroji je **team coaching** a **management training**, aby se dobře moderovaly [ceremonie](https://www.atlassian.com/agile/scrum/ceremonies){:target="_blank" rel="noopener noreferrer nofollow"}, udržovala se morálka, atmosféra a vztahy v týmu.

### Delivering (Agile sustainability)

Tým v této zóně se nezaměřuje jen na obchodní hodnotu, ale také jak ji dostat na trh tak často, jak ji trh přijme. Oproti focusing zóně má nejen schopnost dodávat, ale dodávat, kdy potřebuje. Jedná se o technicky nejintenzivnější zónu. Pro úspěšný přechod do této zóny je důležitá **změna týmových schopností**.

#### Výhody

Hlavní výhodou je nízká míra defektů a vysoká produktivita. Díky tomu je méně nákladů na opravy a více času na dodávání funkcionalit. A jelikož máme větší kontrolu nad dodávku, jsme ji také schopni lépe plánovat.

#### Nástroje a investice

Mezi důležité nástroje patří [DevOps](https://www.atlassian.com/devops){:target="_blank" rel="noopener noreferrer nofollow"}, [UX](https://en.wikipedia.org/wiki/User_experience){:target="_blank" rel="noopener noreferrer nofollow"}, [Extreme Programming](https://www.agilealliance.org/glossary/xp/){:target="_blank" rel="noopener noreferrer nofollow"} a [QA](https://www.atlassian.com/inside-atlassian/qa){:target="_blank" rel="noopener noreferrer nofollow"}. Techniky jako [CI/CD](https://www.atlassian.com/continuous-delivery/continuous-integration){:target="_blank" rel="noopener noreferrer nofollow"}, [dockerizace](https://docs.docker.com/get-started/){:target="_blank" rel="noopener noreferrer nofollow"}, [orchestrace](https://kubernetes.io/){:target="_blank" rel="noopener noreferrer nofollow"}, [monitorování](https://www.elastic.co/elk-stack){:target="_blank" rel="noopener noreferrer nofollow"}, [refaktoring](https://refactoring.guru/refactoring){:target="_blank" rel="noopener noreferrer nofollow"}, [branching model](https://docs.gitlab.com/ee/workflow/gitlab_flow.html){:target="_blank" rel="noopener noreferrer nofollow"}, [code review](https://www.atlassian.com/agile/software-development/code-reviews){:target="_blank" rel="noopener noreferrer nofollow"}, [TDD](https://www.agilealliance.org/glossary/tdd/){:target="_blank" rel="noopener noreferrer nofollow"} a [párové programování](https://www.agilealliance.org/glossary/pairing/){:target="_blank" rel="noopener noreferrer nofollow"} značně pomáhají k technické excelenci. Také nemůžeme opomenout technický trénink a mentorství. Může se jednat o školení, online kurzy nebo i literaturu.

### Optimizing (Agile’s promising)

Tým v této zóně nejen dodává, ale i ví, co trh chce. Také ví nejen, co dodává, ale proč to dodává (viz [golden circle](https://www.toolshero.com/leadership/golden-circle-simon-sinek/){:target="_blank" rel="noopener noreferrer nofollow"} Simona Sinka). Tým si definuje vlastní metriky a plánuje, jak zlepšit pozici na trhu. To znamená, že má tým dostatek prostředků a kompetencí jak k dodávání na trh, tak k práci s trhem. Pro úspěšný přechod do této zóny je důležitá **změna organizační struktury**.

#### Výhody

Přínosem této zóny je vyšší hodnota dodávek, včetně lepšího rozhodování o produktech na trhu. Tým umí získávat zpětnou vazbu a pracovat s ní, reagovat na ni. Vrcholem je pak samotné vedení trhu.

#### Nástroje a investice

Skvělým nástrojem je [lean startup](https://en.wikipedia.org/wiki/Lean_startup){:target="_blank" rel="noopener noreferrer nofollow"} a [lean software development](https://en.wikipedia.org/wiki/Lean_software_development){:target="_blank" rel="noopener noreferrer nofollow"}, jejichž filozofií je eliminace plýtvání a zvýšení adaptability. [Beyond budgeting](https://www.toolshero.com/financial-management/beyond-budgeting/){:target="_blank" rel="noopener noreferrer nofollow"}, který upřednostňuje adaptabilní strategické plánování, a [design thinking](https://www.interaction-design.org/literature/topics/design-thinking){:target="_blank" rel="noopener noreferrer nofollow"}.

### Strengthening (Agile’s feature)

Tato zóna je spíše teoretická, neboť sami autoři nenašli úplné dosažení této zóny. Proto se jí také říká **Agile’s feature**, neboť k ní ještě směřujeme. Cílem je mimo týmové učení a lepší organizační rozhodování. Jsou i některé doporučené nástroje, na které spíše odkážu k oficiálnímu článku.

## Jak jsme na tom my

Naše týmy úspěšně zvládly **focusing** zónu, kde primárně používáme **Scrum** a některé techniky z **Extreme programming**. Ještě pracujeme na definování rozvojových plánů jednotlivců, které by korelovaly s cíli projektů a organizace. Dáváme silný důraz na poskytování osobních zpětných vazeb (kvartálně) a prezenci team lídrů v týmech, kteří udržují morálku a vztahy v týmu. Jako [atlassian platinum partneři](https://www.atlassian.com/partners){:target="_blank" rel="noopener noreferrer nofollow"} používáme jejich nástroje - [Jira](https://atlassian.com/software/jira){:target="_blank" rel="noopener noreferrer nofollow"} a [Confluence](https://atlassian.com/software/confluence){:target="_blank" rel="noopener noreferrer nofollow"}. Pro real-time komunikaci pak používáme [slack](https://slack.com/){:target="_blank" rel="noopener noreferrer nofollow"}.

Většina našich [business jednotek](https://managementmania.com/cs/strategicke-obchodni-jednotky){:target="_blank" rel="noopener noreferrer nofollow"} včetně zakázkového vývoje, ve kterém pracuji, je aktuálně v zóně **delivering**. Zde kontinuálně hledáme prostor k dalšímu technologickému růstu a odlišení od konkurence, naše [USP](https://en.wikipedia.org/wiki/Unique_selling_proposition){:target="_blank" rel="noopener noreferrer nofollow"} (unique selling proposition). Je pro nás důležitá automatizace - CI/CD skrze [Bamboo](https://www.atlassian.com/software/bamboo){:target="_blank" rel="noopener noreferrer nofollow"}, build tool v podobě [maven](https://maven.apache.org/){:target="_blank" rel="noopener noreferrer nofollow"} a [gradle](https://gradle.org/){:target="_blank" rel="noopener noreferrer nofollow"}, inspekce kódu skrze [SonarQube](https://www.sonarqube.org/){:target="_blank" rel="noopener noreferrer nofollow"} a správa balíčků skrze [Nexus Repository](https://www.sonatype.com/nexus-repository-sonatype){:target="_blank" rel="noopener noreferrer nofollow"}. Také používáme další techniky z XP jako code review a nově zavedené párové programování, které usnadňuje onboarding a šíření užitečných praktik při vývoji. VCS hostujeme v [Bitbucketu](https://atlassian.com/software/bitbucket){:target="_blank" rel="noopener noreferrer nofollow"} a repozitáře spravujeme skrze [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow){:target="_blank" rel="noopener noreferrer nofollow"}. Na UX využíváme externí firmy, ale budujeme kompetence i ve vlastních řadách, abychom dokázali skvěle realizovat a doplňovat jejich feedback.

Do **optimizing** zóny se teprve dostáváme v naší produktové business jednotce, kde se snažíme zlepšit práci s trhem. Běžné chování [product ownera](https://www.scrum.org/resources/what-is-a-product-owner){:target="_blank" rel="noopener noreferrer nofollow"} zde nestačí, ještě potřebujeme lepší získávání zpětné vazby a reakci na ni. Včasné zapojení marketingu a digitální agentury nám však pomohlo vychytat vstup našeho nového produktu [amethyst project](https://www.amethyst-project.com/){:target="_blank" rel="noopener noreferrer nofollow"} na trh.

## Zdroje

* [https://www.agilefluency.org/](https://www.agilefluency.org/){:target="_blank" rel="noopener noreferrer nofollow"}
* [https://martinfowler.com/articles/agileFluency.html](https://martinfowler.com/articles/agileFluency.html){:target="_blank" rel="noopener noreferrer nofollow"}
* [Martin Fowler – Agile Essence and Fluency](https://youtu.be/URlnxbaHhTs){:target="_blank" rel="noopener noreferrer nofollow"}
* [https://www.thoughtworks.com/insights/blog/road-mapping-your-way-agile-fluency](https://www.thoughtworks.com/insights/blog/road-mapping-your-way-agile-fluency){:target="_blank" rel="noopener noreferrer nofollow"}
