---
layout: post
title: Nenechte se řídit svým egem
description: Jak lehké je uvěřit vlastní výjimečnosti?

permalink: /blog/nenechte-se-ridit-svym-egem
cover: /assets/images/2018-10-15/cover.jpg
image: /assets/images/2018-10-15/cover.jpg
---


_Zenový mistr byl navštíven mladým filozofem, který cestoval z daleka, aby se s ním mohl setkat. Mistr souhlasil, že se s ním setká, protože měl na filozofa spoustu doporučení od jeho učitelů. Sedli si spolu pod strom ke konverzaci a téma se rychle stočilo k tomu, co může mistr naučit mladého filozofa. Rozpoznávající žár mládí se mistr vřele usmál a začal popisovat své meditační techniky. Byl však rychle přerušen filozofem, který řekl: „Ano, rozumím tomu, co říkáte! Dělali jsme podobnou techniku v chrámu, ale používali jsme raději obrazy k soustředění.”_

_Jakmile skončil filozof vysvětlovat mistrovi, jak byl učen a jak cvičil svou meditaci, začal mistr znovu mluvit. Tentokrát se pokusil mladému muži říct, jak by se jeden měl sladit s přírodou a vesmírem. Nestihl doříct ani dvě věty, když ho filozof znovu přerušil a začal mluvit o tom, jak byl učen k meditaci a tak dále._

_Znovu mistr trpělivě počkal, až mladý filozof skončí se svým vzrušeným vysvětlováním. Když filozof opět utichl, promluvil mistr o pozorování humoru ve všech situacích. Mladý muž neztratil ani chvilku a začal mluvit o svých oblíbených vtipech a jak si myslel, že by se daly spojit se situacemi, kterým čelil._

_Když filozof skončil, pozval ho mistr dovnitř na čajovou ceremonii. Filozof radostně souhlasil, neboť slyšel, že mistr provádí ceremonie jako nikdo jiný. Taková příležitost byla vždy obrovskou výsadou. Uvnitř si mistr počínal bez chyby, dokud nezačal nalévat čaj do hrnku. Jak mistr naléval, všiml si filozof, že je hrnek naplněn více, než je běžné. Mistr nepřestal nalévat čaj a hrnek byl brzy plný až po okraj. Mladý muž nevěděl co říct, pouze v údivu zíral na mistra. Mist stále naléval, jako by se nic špatného nedělo, a hrnek začal přetékat, rozlévajíce horký čaj na podlahu a mistrovo hakama. Filozof, nevěříc tomu, co zrovna vidí, konečně pronesl: „Přestaň nalévat! Nevidíš, že je ten hrnek už plný a přetéká?”._

_S těmito slovy mistr jemně položil konvici na oheň a podíval se na mladého filozofa s jeho všudypřítomným úsměvem a řekl: “Pokud za mnou přijdeš s hrnkem, který je už úplně plný, jak můžeš očekávat, že ti dám něco k pití?”._

(Hoover, Dave H., and Adewale Oshineye. Apprenticeship Patterns: Guidance for the Aspiring Software Craftsman. O'Reilly, 2010.)

<br/>

Předchozí příběh ukazuje chybu, které se většina z nás občas dopouští. Jsme tak moc přesvědčení o své pravdě a výjimečnosti, až přehlížíme svou slepotu vůči našemu okolí. Tím sami sebe okrádáme o příležitosti, které se před námi objevují. 

Začínal jsem pracovat jako programátor dříve jak většina kamarádů na střední. Jako pracující programátor jsem se rychle naučil věci, kterým tehdy kamarádi moc nerozuměli. Takový náskok dokáže člověku rychle stoupnout do hlavy, nemluvě o samotném věkovém rozdílu s kolegy. Vždy jsem byl hladový po znalostech a sebezlepšování, takže jsem začal postupně v jistých věcech přerůstat své kolegy. Samozřejmě se hloubka a šířka znalostí nedala tehdy srovnávat, ale nacházel jsem lepší slova a poukazoval na možnosti zlepšení. Když jsem poté nastoupil na vysokou školu, přišlo mi všechno hrozně pomalé a zbytečné. Nejspíš mě tehdy nemohl nikdo dobře připravit na to, že je rychlost výuky na vysoké škole úplně něco jiného. Kvůli negativnímu pocitu ze začátku kurzů jsem se připravil o úžasné vědomosti, které se typicky probírali později.

Obrovský zvrat u mě nastal, když jsem do týmu v práci, který jsem vedl, mohl přivítat kluka, který byl mladší a mnohem schopnější programátor než já. Byl to zvláštní pocit, ale cítil jsem obrovskou radost, že mohu mít takového člověka v týmu. Nepřišlo to hned, ale v určité chvíli jsem si uvědomil, že i když jsem v jistých věcech tolik napřed, vůbec na tom nezáleží, protože mi každý člověk může něco předat. Záleží pouze na tom, co si od nich budu chtít odnést. Neustále mám co zdokonalovat, neustále se mám co učit. 

Věk není měřítkem dovedností, proto bychom neměli ignorovat rady mladých. Občas se mi stávalo, že nadřízení úplně nedůvěřovali tomu, co říkám. Mé schopnosti v přesvědčování a argumentaci tak byly a stále jsou zkoušeny, což přirozeně poskytuje úžasné zkušenosti. Na druhou stranu se nemohu ubránit pocitu nepochopení míry mého chápání, neboť jsem mohl pomoci nejméně jedné firmě k růstu.


> “Nikdy se nepřestávejte učit, protože život nikdy učit nepřestane.”


## Zdroje
* [http://www.clearintentions.net/emptying-your-cup/](http://www.clearintentions.net/emptying-your-cup/){:target="_blank" rel="noopener noreferrer nofollow"}
* [https://www.rickhanson.net/empty-the-cup/](https://www.rickhanson.net/empty-the-cup/){:target="_blank" rel="noopener noreferrer nofollow"}
* [https://www.amazon.com/Apprenticeship-Patterns-Guidance-Aspiring-Craftsman/dp/0596518382](https://www.amazon.com/Apprenticeship-Patterns-Guidance-Aspiring-Craftsman/dp/0596518382){:target="_blank" rel="noopener noreferrer nofollow"}

