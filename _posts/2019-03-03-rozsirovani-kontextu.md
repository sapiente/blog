---
layout: post
title: Rozšiřování kontextu
description: Jak vidět větší část nebe? Jak můžeme zvýšit naši znalost a zlepšit vnímání?

permalink: /blog/rozsirovani-kontextu
cover: /assets/images/2019-03-03/cover.jpg
image: /assets/images/2019-03-03/cover.jpg
---


> Byla jednou jedna žába, která žila od narození na dně studny. Celý život tak mohla vidět pouze část oblohy, a tak uvěřila, že je nebe pouze tak velké, jako díra studny. 
>
> Je ale nebe velká pouze jako díra studny?

> We think too small, like the frog at the bottom of the well. He thinks the sky is only as big as the top of the well. If he surfaced, he would have an entirely different view.  
> -- Mao Zedong 

Rád tento krátký příběh vyprávím, protože jasně ukazuje, že omezení existují. Nemyslím tím limity, ale překážky stojící v našich cestách. Problém je, že si jich nemusíme být vědomi, vidět je. Nemusíme v dané fázi našeho života vědět, co nás brzdí od dalšího růstu, ať už profesního neho osobního. Samotná nevědomost toho, co by nám pomohlo, nás brzdí.

> There are unknown unknowns - things we don’t know that we don’t know.  
>  -- Donald Rumsfeld

Nalezení neznámých neznámých je důležité například i v projektovém řízení, aby se z neznámých rizik stala rizika známá a bylo možné zavést patřičná opatření (viz. [characterizing unknown unknowns](https://www.pmi.org/learning/library/characterizing-unknown-unknowns-6077){:target="_blank" rel="noopener noreferrer nofollow"}). 

## Paradigma

Paradigma je pojem označující způsob, jakým vidíme svět. Ne ve smyslu našeho zraku, ale ve smyslu přijímání, chápání a vysvětlování si (Covey, 2004, s. 10).

Covey přirovnává paradigma k mapě, která nám vysvětluje dané teritorium. Pokud se chceme někam dostat a máme špatnou mapu, tak byť bychom pracovali sebevíce a sebelépe, dostali bychom se na špatné místo. Je to známý problém s rozlišením účinnosti (efficiency) a účelnosti (effectiveness).

> Efficiency is doing things right, effectiveness is doing the right thinks.  
> -- Peter Drucker

V rámci HEXACO modelu se na obchodním a profesním úspěchu nejvíce podílí svědomitost (conscientiousness), ve které je faktor prozíravost/vhled (prudence). Tai Lopez ve svém rozhovoru [Why Grinding Isn’t Enough](https://youtu.be/jX5eajzLJMU){:target="_blank" rel="noopener noreferrer nofollow"} tento faktor přirovnává ke kompasu. Kdyby byl náš cíl kousíček před námi, ale náš kompas byl obrácený, museli bychom k jeho dosažení obejít celou zeměkouli (~40k km). Kdyby byl ale nastavený správně, ušli bychom pouze pár kroků a dosáhli jej.

Pokud tedy máme nějaké vnímání světa, nějakou konkrétná mapu a nastavený kompas, je těžké vidět překážky a možnosti mimo jejich kontext. A stejně jako je [většina příležitostí mimo komfortní zónu](https://medium.com/the-mission/why-the-magic-happens-when-you-step-out-of-your-comfort-zone-8698ccfbfc6a){:target="_blank" rel="noopener noreferrer nofollow"}, je i většina dobrých nápadů mimo náš kontext vnímání. Otázka tedy zní, jak můžeme rozšířit náš kontext vnímání?

## Rozšíření kontextu

Změně způsobu našeho vnímání se říká [paradigma shift](https://resources.franklincovey.com/blog/paradigms){:target="_blank" rel="noopener noreferrer nofollow"}. Základem pro jakoukoli změnu je otevřenost vůči ní, stejně jako je [přiznání si problému prvním krokem k jeho vyřešení](https://hrdailyadvisor.blr.com/2013/02/11/admitting-you-have-a-problem-is-the-first-step-in-fixing-the-problem/){:target="_blank" rel="noopener noreferrer nofollow"}. Jakmile změnu chceme, zůstává otázka - jak se k ní dostat?

### Hledání jádra

O technice [5 why’s](https://www.mindtools.com/pages/article/newTMC_5W.htm){:target="_blank" rel="noopener noreferrer nofollow"} jste již nejspíše slyšeli, zmiňoval jsem ji i v [minulém článku](/agile-fluency-model){:target="_blank" rel="noopener noreferrer nofollow"}. Jedná se o výslechovou techniku, jejíž snahou je najít jádro problému. Často máme tendenci soustředit se pouze na viditelné příčiny a aplikovat rychlé záplaty. Jenže mnohdy je právě jádrem problému něco skrytého. Něco, co se samo o sobě zdá neškodné. Něco, co třeba ani nevíme, že existuje. Pokud budeme plevel pouze sekat, vždy znovu doroste. Pokud jej ale vyrveme i s kořeny, přestane růst. 

Tato technika neřeší naši neznalost, ale pomáhá redukovat naši předpojatost a dostat se systematicky a objektivně k dlouhotrvajícímu řešení.


### Sdílení znalostí

V poslední době se mě několik lidí ptalo, proč tak moc čtu a sdílím. Rovnou bych na to rád odpověděl. Pro mě čtení znamená možnost rozšíření mého kontextu a sdílení způsobu, jak toto rozšíření poskytnout jiným. 

Když jsem poprvé objevil krásu seberozvoje, pocítil jsem i její stinnou stránku - vaše okolí vám najednou začne připadat nedokonalé, případně ještě více, než předtím. A jelikož jste začali objevovat možná řešení na dané problémy, lepší způsoby, začnete lidem radit, snažit se je posouvat a pomáhat. Jenže ne všichni si chtějí nechat pomoci, né všichni mají otevřenou mysl a chuť růst. Nebo mají, ale nejsou ještě připraveni.

Po této důležité zkušenosti jsem ze své hodnoty růstu nepolevil, jen jsem se začal více soustředit na sebe. Když okolí bude chtít, řekne si o radu, a já rád pomohu. Jenže jak si může někdo říci o radu, když nevidí problém či prostor ke zlepšení? 

Mezi těmito dvěma cestama jsem našel zlatou střední - sdílení. Mohu lidem poskytnout to, co pomohlo mně, aniž bych jim vnucoval danou věc. Nenásilnou cestou jim ukazuji možnost, aniž bych je dostával do nepříjemné situace, ve které by byli zranitelní a museli přiznat, že daný problém či nedokonalost mají. Sdílením je totiž toto objevení čistě na nich a mohou si na něj vzít tolik času, kolik potřebují. Často stačí jedna myšlenka či otázka, která zažehne touhu po něčem lepším, po něčem více.

Sdílení je tedy způsob, jak ovlivnit vnímání a kontext někoho jiného. Nemusíme každý psát knihy, či články na blog. Každý ale můžeme sdílet ty, které nás ovlivnily a něco nám přinesly. Tím dáváme možnost ostatním, a i když začneme sami, dříve či později se přidá i někdo další. Někdo, kdo poskytne stejnou možnost zase nám.

## Jak si rozšiřuji kontext

Osobně pro rozšiřování kontextu využívám tyto zdroje:
- knihy
- videa - přednášky, konference, podcasty
- články

Každý zdroj pak rozděluji na kategorie:
- management & leadership
- seberozvoj
- software development

Základní a nikterak překvapující je četba knih. Věřím, že je pro osobní a profesní růst velice důležitá, protože knihy obsahují myšlenky a výzkumy lidí, kteří se v daných doménách pohybují/pohybovali. Nejsou vždy zcela relevantní nebo užitečné, ale jejich absence rozhodně ničemu nepomáhá, proto je i minimální přínos lepší než nic.

Dalším zdrojem jsou videa, primárně z YT. Jedná se jak o krátké vlogy, tak konference a přednášky. Druhou složkou jsou podcasty, které rád poslouchám cestou do práce a z ní. Neposlouchám je každý den, rád cestou přemýšlím a jsem sám se sebou, taková hezká příležitost k sebereflexi a rekapitulaci buď denního plánu, nebo již uplynulého dne.

Posledním zdrojem jsou články. K jejich sledování používám nástroj [feedly](https://feedly.com){:target="_blank" rel="noopener noreferrer nofollow"}, který mi hlídá nové publikace z různých stránek.

Rád bych více zapojil rozšiřování kontextu skrze osobní diskuzi, ale krom meetupů je těžké najít mentora či skupinu stejně smýšlejících lidí v růstu a sdílení.

## Jak sdílím

Většina real-time komunikačních nástrojů, které IT firmy používají, umožňují vytvářet tzv. channely. Ty se dají využít pro komunikace k danému projektu či doméně/tématu. Bohužel je tu riziko rozptylování. Spousta firem již naráží na potřebu separování real-time a asynchronní komunikace. Sdílení odkazů je ale většinou bez diskuzí a lidé je čtou s odstupem, proto je využití real-time komunikačního nástroje přirozenou a bezproblémovou volbou. Doporučuji vytvořit channely pro jednotlivé kategorie, aby se dal lépe cílit obsah a mohlo jej sledovat více lidí.

## Zdroje

- COVEY, S. R. (2004). The 7 habits of highly effective people: restoring the character ethic. New York, Free Press.
