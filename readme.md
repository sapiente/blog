# Running
- run `docker-compose up -d`
- run `docker-compose logs -f`

# Jekyll
- [documentation](https://jekyllrb.com/docs/)
- [tutorial](https://www.youtube.com/playlist?list=PLLAZ4kZ9dFpOPV5C5Ay0pHaa0RJFhcmcB)
